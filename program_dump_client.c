#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 5000
  
char root[1024] = "../databases";

int check_userpass(char *user, char *pass)
{
    char path[1024];
    strcpy(path, root);
    strcat(path, "/users/user_list"); // Database 'users', Tabel 'user_list'

    FILE *table = fopen(path, "r");
    char line[1024]; 
    char *saveptr_user;
    int found = 0;
	int counter = 0;
	int tok_counter = 0;

    while (fgets(line, sizeof(line), table)) {
        if (counter < 3) {
            counter++;
            continue;
        }

        char *token = strtok_r(line, " ", &saveptr_user); // ganti ini pake fungsi yang bisa nyari substring
        token = strtok_r(NULL, " ", &saveptr_user); 

        if (strcmp(token, user) == 0) {
            found = 1;

            token = strtok_r(NULL, " ", &saveptr_user); 
            token = strtok_r(NULL, " ", &saveptr_user); 
            if (strcmp(token, pass) == 0) found = 2;
            break;
        }
    }

    fclose(table);

    return found;
}

int main(int argc, char const *argv[]) {
    char username[200];
    char password[200];
    char database[200];
    int checker;
    int success = 0;

    if(argv[1] != NULL && strcmp(argv[1], "-u") == 0)
    {
        if(argv[2] != NULL)
        {
            if(argv[3] != NULL && strcmp(argv[3], "-p") == 0)
            {
                if(argv[4] != NULL && argv[5] != NULL)
                {
                    strcpy(username, argv[2]);
                    strcpy(password, argv[4]);
                    strcpy(database, argv[5]);
                    checker = check_user_pass(username, password);

                    if(checker == 1)
                    {
                        // salah pass
                        printf("Wrong password.\n");
                    }
                    
                    else if(checker == 2)
                    {
                        // success
                        success = 1;
                        FILE *src, *target;
                        char ch;
                        char src_path[200];

                        strcpy(src_path, "../databases/history/");
                        strcat(src_path, argv[5]);

                        src = fopen(src_path, "r");
                        
                        while ((ch = fgetc(src)) != EOF)
                            printf("%c", ch);

                        fclose(src);
                    }
                    
                    else if (checker == 0) printf("Username not found.\n");
                }
                
                else printf("Please input your username and password.\n");
            }
            
            else printf("Please input your username and password.\n");
        }
        
        else printf("Please input your username and password.\n");
    }
    
    else printf("Please input your username and password1.\n");
    
}
