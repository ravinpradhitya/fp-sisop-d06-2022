#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 5000


char root[1024] = "../databases";

int check_userpass(char *user, char *pass) {
    char path[1024];
    strcpy(path, root);
    strcat(path, "/users/user_list"); // Database 'users', Tabel 'user_list'

    FILE *table = fopen(path, "r");
    char line[1024]; 
    char *saveptr_user;
    int found = 0;
	int counter = 0;
	int tok_counter = 0;

    while (fgets(line, sizeof(line), table)) {
        if (counter < 3) {
            counter++;
            continue;
        }

        char *token = strtok_r(line, " ", &saveptr_user); // ganti ini pake fungsi yang bisa nyari substring
        token = strtok_r(NULL, " ", &saveptr_user); 

        if (strcmp(token, user) == 0) {
            found = 1;

            token = strtok_r(NULL, " ", &saveptr_user); 
            token = strtok_r(NULL, " ", &saveptr_user); 
            if (strcmp(token, pass) == 0) found = 2;
            break;
        }
    }

    fclose(table);

    return found;
}
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    // Checking if sudo
    uid_t uid=getuid(), euid=geteuid();
    int sudo;
    
    if(geteuid() != 0) {
        char unsudo[] = "unsudo";
        send(sock , unsudo, strlen(unsudo), 0 );
        sudo = 0;
    }
    
    else {
        char sudo_msg[] = "sudo";
        send(sock , sudo_msg, strlen(sudo_msg), 0 );
        sudo = 1;
    }

    // Asking for username and password if not sudo
    char username[60]; 
    char password[60];
    char msg[1024];
    int success = 0;
    int checker = 0;

    if(sudo == 0)
    {
        if(argv[1] != NULL)
        {
            if(argv[2] != NULL && strcmp(argv[1], "-u") == 0)
            {
                if(argv[3] != NULL && strcmp(argv[3], "-p") == 0)
                {
                    if(argv[4] != NULL)
                    {
                        strcpy(username, argv[2]);
                        strcpy(password, argv[4]);
                        checker = check_userpass(username, password);

                        if(checker == 1)
                        {
                            // salah pass
                            printf("Wrong password.\n");
                        }
                        else if(checker == 2)
                        {
                            // success
                            success = 1;
                            printf("Log in successful.\n");
                        }
                        else if (checker == 0) printf("Username not found.\n");
                    }
                    else printf("Please input your username and password.\n");
                }
                else printf("Please input your username and password.\n");
            }
            else {
                printf("Please input your username and password.\n");
            }
        }
        else printf("Please input your username and password1.\n");

        if (success == 0) {
            strcpy(msg, "Log in failed");
            send(sock, msg, strlen(msg), 0);
            exit(1);
        }
        else send(sock, username, strlen(username), 0);
    }

    printf("XXX==================================XXX\n");
    printf(" REVOLUTIONARY ARMY BACKUP DATA STORAGE \n");
    printf("XXX==================================XXX\n");

    while(1) {
        char err_msg[1024] = {0};
        char input[100];
        memset(input, 0, sizeof input);
        scanf("%[^\n]%*c", input);
        send(sock, input, strlen(input), 0);
        read(sock, err_msg, 1024);
        printf("%s\n", err_msg);
    }
}
